package gui;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import searchEngine.TextSearcher;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;

public class MainSceneController {

    @FXML
    private MenuItem searchMenuItem;

    @FXML
    private Button nextButton;

    @FXML
    private Button prevButton;

    @FXML
    private Label label;

    @FXML
    private TextField searchTextField;

    @FXML
    private TreeView<TreeItemFile> fileTree;

    @FXML
    private TabPane tabPane;

    private Stage primaryStage;

    private TextSearcher searcher;

    private SearchTabsHandler tabsHandler;

    private String fileType;

    @FXML
    private void initialize(){
        searchTextField.setVisible(false);

        nextButton.setTooltip(new Tooltip("Next Occurrence"));
        prevButton.setTooltip(new Tooltip("Previous Occurrence"));

        tabPane.setTabClosingPolicy(TabPane.TabClosingPolicy.SELECTED_TAB);

        fileType = "log";

        searchTextField.setOnKeyPressed(new StartSearchEventHandler());
        fileTree.setOnMouseClicked(new OpenTabEventHandler());
    }

    @FXML
    private void chooseFolder() {
        DirectoryChooser dirChooser = new DirectoryChooser();

        dirChooser.setTitle("Select the log folder");
        dirChooser.setInitialDirectory(new File(System.getProperty("user.home")));

        File selectDir = dirChooser.showDialog(primaryStage);

        if(selectDir != null) {
            TreeItemFile itemFile = new TreeItemFile(Paths.get(selectDir.toURI()));
            fileTree.setRoot(new TreeItem<>(itemFile));
            fileTree.setShowRoot(false);

            searcher = new TextSearcher(itemFile.getURI());

            searchTextField.setVisible(true);
            searchMenuItem.setDisable(false);

            label.setText("Selected directory:\n" + selectDir.getAbsolutePath());
        }
    }

    @FXML
    private void searchLine() {
        tabsHandler.scrollToSearchText();
    }

    @FXML
    private void searchMenuItem() {
        searchTextField.requestFocus();
    }

    @FXML
    private void fileTypeAction() {
        TextInputDialog dialog = new TextInputDialog(fileType);

        dialog.setHeaderText(null);
        dialog.setTitle("File type");
        dialog.setContentText("Enter file type:");
        dialog.showAndWait().ifPresent(type -> {
            fileType = type;
            clearTreeView();
        });
    }

    void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    private void clearTreeView() {
        if (fileTree.getRoot() != null && fileTree.getRoot().getChildren().size() > 0) {
            fileTree.getRoot().getChildren().clear();
            tabPane.getTabs().clear();
        }
    }

    //TODO: Сделать проверку строки на наличие хотябы одного видимого символа
    private class StartSearchEventHandler implements EventHandler<KeyEvent> {

        @Override
        public void handle(KeyEvent keyEvent) {
            if(keyEvent.getCode().equals(KeyCode.ENTER) && searchTextField.getText().length() > 0) {
                String searchText = searchTextField.getText();
                tabsHandler = new SearchTabsHandler(tabPane, searchText);

                clearTreeView();

                Set<Path> files = searcher.searchText(searchText, fileType);

                if (files.size() > 0) {
                    TreeViewUtil.addFilesInTreeView(fileTree, files);
                } else {
                    Alert alert = new Alert(Alert.AlertType.WARNING);

                    alert.setTitle(null);
                    alert.setHeaderText("No files found");
                    alert.setContentText("Search options:\n" +
                            "Search text - " + searchText + "\n" +
                            "File type - " + fileType);
                    alert.show();
                }
            }
        }
    }

    private class OpenTabEventHandler implements EventHandler<MouseEvent> {

        @Override
        public void handle(MouseEvent mouseEvent) {
            if(mouseEvent.getButton().equals(MouseButton.PRIMARY) && mouseEvent.getClickCount() == 2){
                TreeItem<TreeItemFile> selectedItem = fileTree.getFocusModel().getFocusedItem();

                if(selectedItem.isLeaf()) {
                    tabsHandler.activeTab(selectedItem);
                    nextButton.setDisable(false);
                }
            }
        }
    }
}