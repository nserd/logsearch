package gui;

import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class TreeViewUtil {
    private static TreeView<TreeItemFile> view;

    static void addFilesInTreeView(TreeView<TreeItemFile> view, Set<Path> files) {
        TreeViewUtil.view = view;

        Path rootPath = view.getRoot().getValue().getURI();
        List<String> relPaths = getRelativePaths(files);

        String[][] filenames = new String[relPaths.size()][];

        for (int i = 0; i < filenames.length; i++) {
            filenames[i] = relPaths.get(i).split(File.separator);
        }

        for (int i = 0; i < filenames.length; i++) {
            StringBuilder[] pathsStrings = new StringBuilder[filenames.length];
            pathsStrings[i] = new StringBuilder(rootPath.toString());

            for (int j = 0; j < filenames[i].length; j++) {
                Path parentPath = Paths.get(pathsStrings[i].toString());

                pathsStrings[i].append(File.separator);
                pathsStrings[i].append(filenames[i][j]);

                Path path = Paths.get(pathsStrings[i].toString());

                TreeItem<TreeItemFile> item = new TreeItem<>(new TreeItemFile(path));
                TreeItem<TreeItemFile> parentItem = getParent(view.getRoot().getChildren(), parentPath, j);

                addItem(parentItem, item);
            }
        }

        files.forEach(System.out::println); // debug
    }

    private static void addItem(TreeItem<TreeItemFile> parentItem, TreeItem<TreeItemFile> item) {
        if (parentItem == null) {
            if (!contains(view.getRoot().getChildren(), item)) {
                view.getRoot().getChildren().add(item);
            }
        } else if (!contains(parentItem.getChildren(), item)) {
            parentItem.getChildren().add(item);
        }
    }

    // TODO: test depth
    private static TreeItem<TreeItemFile> getParent(List<TreeItem<TreeItemFile>> list, Path parentPath, int depth) {
        if (depth < 0) return null;

        TreeItem<TreeItemFile> item = null;

        //можно добавить break для того, чтобы работало быстрее
        for (TreeItem<TreeItemFile> listItem : list) {
            Path itemPath = listItem.getValue().getURI();
            item = itemPath.equals(parentPath) ? listItem : getParent(listItem.getChildren(), parentPath, depth - 1);
        }

        return item;
    }

    private static boolean contains(List<TreeItem<TreeItemFile>> list, TreeItem<TreeItemFile> item) {
        Path path = item.getValue().getURI();
        return list.stream().anyMatch(treeItem -> treeItem.getValue().getURI().equals(path));
    }

    private static List<String> getRelativePaths(Set<Path> files) {
        Path root = view.getRoot().getValue().getURI();

        return files.stream()
                .map(x -> x.toString().replace(root.toString() + File.separator, ""))
                .collect(Collectors.toList());
    }
}