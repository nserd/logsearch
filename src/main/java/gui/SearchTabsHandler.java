package gui;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ListView;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

public class SearchTabsHandler {
    private final TabPane tabPane;
    private final String searchText;

    private Tab selectedTab;

    SearchTabsHandler(TabPane tabPane, String searchText) {
        this.tabPane = tabPane;
        this.searchText = searchText;
    }

    public void activeTab(TreeItem<TreeItemFile> selectedItem) {
        Tab tab = getTab(selectedItem);

        if (tab != null) {
            tabPane.getSelectionModel().select(tab);
        } else {
            Tab newTab = createTab(selectedItem.getValue().getURI());

            tabPane.getTabs().add(newTab);
            tabPane.getSelectionModel().select(newTab);
        }

        selectedTab = tab;
    }

    private Tab createTab(Path uri) {
        Tab tab = new Tab(uri.toString());

        ObservableList<TextFlow> ol = FXCollections.observableArrayList();
        ListView<TextFlow> listView = new ListView<>(ol);

        try {
            Files.lines(uri).forEach(line -> {
                TextFlow textFlow = new TextFlow();

                if (line.contains(searchText)) {
                    textFlow.getChildren().addAll(parseLine(line));
                } else {
                    textFlow.getChildren().add(new TextMonospaced(line));
                }

                ol.add(textFlow);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        listView.setFocusTraversable(false);
        listView.setFixedCellSize(23); // fixed scroll problem

        tab.setContent(listView);

        return tab;
    }

    void scrollToSearchText() {
        if (selectedTab != null) {
            @SuppressWarnings("unchecked")
            ListView<TextFlow> listView = (ListView<TextFlow>) selectedTab.getContent();
            listView.scrollTo(searchTextFlow(listView));
        }
    }

    private TextFlow searchTextFlow(ListView<TextFlow> listView) {
        return listView.getItems().stream().filter(textFlow -> {
            ObservableList<Node> os = textFlow.getChildren();
            for (Node n : os) {
                Text text = (Text) n;

                if (text.getText().equals(searchText))
                    return true;
            }

            return false;
        }).findFirst().orElse(null);
    }

    private List<Text> parseLine(String line) {
        List<Text> list = new LinkedList<>();

        StringBuilder sb = new StringBuilder(line);

        int index;
        while ((index = sb.indexOf(searchText)) != -1) {
            list.add(new TextMonospaced(sb.substring(0, index)));

            TextMonospaced highlight = new TextMonospaced(searchText);
            highlight.setFill(Color.RED);
            highlight.setStyle("-fx-font-weight: bold");

            list.add(highlight);
            sb.delete(0, index + searchText.length());
        }

        if (sb.length() > 0) {
            list.add(new TextMonospaced(sb.toString()));
        }

        return list;
    }

    private Tab getTab(TreeItem<TreeItemFile> item) {
        for (Tab tab : tabPane.getTabs()) {
            if (tab.getText().equals(item.getValue().getURI().toString())) {
                return  tab;
            }
        }

        return  null;
    }
}
