package gui;

import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class TextMonospaced extends Text {
    TextMonospaced() {
        super();
        this.setFont(Font.font(java.awt.Font.MONOSPACED));
    }

    TextMonospaced(String textString) {
        this();
        setText(textString);
    }
}
