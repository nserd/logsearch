package gui;

import java.nio.file.Path;

class TreeItemFile {
    private final Path uri;

    TreeItemFile(Path uri) {
        this.uri = uri;
    }

    Path getURI() {
        return uri;
    }

    @Override
    public String toString(){
        return uri.getFileName() == null ? "" : uri.getFileName().toString();
    }
}
