package searchEngine;

import java.io.IOException;
import java.nio.charset.MalformedInputException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

class LogFileVisitor extends SimpleFileVisitor<Path> {
    private static final Logger LOGGER = Logger.getLogger(LogFileVisitor.class.getName());

    private final String text;
    private final String fileType;

    private final Set<Path> foundFiles;

    LogFileVisitor(String text, String fileType) {
        this.text = text;
        this.fileType = fileType;

        foundFiles = new LinkedHashSet<>();
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        if (!Files.isDirectory(file) && getFileType(file).equals(fileType)) {
            List<String> lines;

            try {
                lines = Files.readAllLines(file);
            } catch (MalformedInputException e) {
                LOGGER.log(Level.INFO, "Malformed Input. File: {0}", file);
                return FileVisitResult.CONTINUE;
            } catch (AccessDeniedException e) {
                LOGGER.log(Level.WARNING, "Access Denied. File: {0}", file);
                return FileVisitResult.CONTINUE;
            }

            if (lines.stream().anyMatch(line -> line.contains(text))) {
               foundFiles.add(file);
            }
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        if (exc instanceof AccessDeniedException) {
            LOGGER.log(Level.WARNING, "Access Denied: {0}", exc.getMessage());
            return FileVisitResult.SKIP_SUBTREE;
        }

        throw exc;
    }

    private String getFileType(Path file) {
        String nameFile = file.getFileName().toString();

        if (nameFile.contains(".") && nameFile.toCharArray()[0] != '.') {
            return nameFile.substring(nameFile.lastIndexOf('.') + 1);
        }

        return "";
    }

    Set<Path> getFoundFiles() {
        return foundFiles;
    }
}