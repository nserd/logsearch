package searchEngine;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TextSearcher {
    private static final Logger LOGGER = Logger.getLogger(TextSearcher.class.getName());

    private final Path logDir;

    public TextSearcher(Path logDir) {
        this.logDir = logDir;

        LOGGER.setLevel(Level.WARNING);
    }

    public Set<Path> searchText(String text, String fileType) {
        Set<Path> foundFiles = new LinkedHashSet<>();

        try {
            LogFileVisitor fileVisitor = new LogFileVisitor(text, fileType);

            Files.walkFileTree(logDir, fileVisitor);
            foundFiles = fileVisitor.getFoundFiles();
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "IOException", e);
            e.printStackTrace();
        }

        return foundFiles;
    }

    public Path getLogDir() {
        return logDir;
    }
}